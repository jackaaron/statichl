# Static Highlight
Convert dynamically styled code and syntax highlighting to static html+css.

## Purpose and Motivation
Libraries like [highlightjs](https://highlightjs.org/static/demo/) and [prismjs](https://prismjs.com/#examples) make code highlighting fast, easy, and pretty.
Unfortunately they require clients to load and run javascript each time pages are rendered, even when page content doesn't change.
**staticHL** captures dynamic codeblock styling, e.g., highlightjs-rendered content, and concretizes<sup>1</sup> that styling in static elements.
The result is 1:1 fidelity with library-generated highlighting, served without javascript.

## Install
```
pip3 install staticHL
```

## Usage
*for a quick working example see the [demo](#demo) below*

```
usage: statichl [-h] [-o OUTPUT_DIR] [-f] [-d DELAY] target

positional arguments:
  target                url or filesystem path containing target content

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT_DIR, --output-dir OUTPUT_DIR
                        write concretized html+css to directory OUTPUT_DIR, defaults to the current directory if unspecified
  -f, --force           force overwrite; overwrite existing html+css files
  -d DELAY, --delay DELAY
                        wait DELAY seconds before parsing to allow resources to load, rationals and sub-second values allowed
```

## Demo
This repo contains a demo html page styled with highlightjs: [dynamic.html](./demo/dynamic.html). Visiting it in a browser 
produces the rendered result of the **hljs** library. You can also run **staticHL** on this page to produce a concretized version and compare the results. 

Download the [demo page](./demo/dynamic.html) and run:
```shell
statichl path/to/dynamic.html
```

open the resulting `./static.html` in a browser and inspect the differences in devtools.

### Side-by-side: rendered output
![rendered](./demo/res/sbs_rendered.png)

### Side-by-side: sources view
![rendered](./demo/res/sbs_sources.png)


## Features
* true-to-source highlighting
* straightforward and granular control over element styling. The goal here is to make ex post facto style edits unnecessary, but easy to perform should you want to
* no loading or execution of external js resources
    * faster content paint/page load
    * sleeping soundly at night
* compatible with any page you can open in a browser

## Anti-features
*in the interest of getting something out I ~~gave up on~~  avoided implementing features where the juice wasn't worth the time/complexity squeeze*
* **in-place document modification**: html elements outside of `<code>` tags, and pre-existing html element attributes do not survive to concretized output
* **responsive units**: staticHL uses selenium to pull style properties. These properties are computed directly from the live page. This produces `px` units regardless of the units specified in source css
* **nested `<code>` elements**: I can't think of a reason someone would do this, but I haven't put any serious thought into what behavior it might produce


## Future development
* **performance improvements:** Fetching or computing page content/properties with selenium is expensive<sup>2</sup>
Ideally we can fetch the raw computed content once with selenium, and then pass it to lxml or BeautifulSoup &mdash; see #14.
Concurrency is very tempting but I plan to resist until the base implementation is faster
* **API**: no functions are exported at the package level; intended use is currently limited to the command line
* **Quality of life improvements:** see #15, #18, #1, #4, #6 

## Troubleshooting
> `<code>` elements aren't being detected
* First make sure the element tag is actually `<code>`. Some wordpress plugins or static site generators don't serve code in `<code>` tags
* Use the `-d` option from [Usage](#usage) to specify a delay to let the page finish loading, see #18 for a (planned) better solution

> The concretized output is mostly correct, but is missing some key attribute or formatting
* We're likely overlooking some CSS property that we should be duplicating, please open an issue
---
### Footnotes
**<sup>1</sup>** I use (or abuse) 'concretize' in the program analysis sense, i.e., to determine a concrete value from an abstract set of possible states. For the actual definition see [abstract interpretation](https://en.wikipedia.org/wiki/Abstract_interpretation#:~:text=A%20semantics%20is%20a%20mathematical%20characterization%20of%20a%20possible%20behavior%20of%20the%20program.%20The%20most%20precise%20semantics%2C%20describing%20very%20closely%20the%20actual%20execution%20of%20the%20program%2C%20are%20called%20the%20concrete%20semantics.)

**<sup>2</sup>** up to ~70% of runtime, tested on the included demo page, which contains only `<code>` tags